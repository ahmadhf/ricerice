CC=c99
CFLAGS=-Wall

PROG=ricerice
PREFIX=/usr/local/

SRCS=$(wildcard src/*.c)
INCL=$(wildcard include/*.h)

run: build
	bin/$(PROG)
build:
	$(CC) $(SRCS) $(INCL) -o bin/$(PROG) -I include/
clean:
	rm bin/*
install: build
	cp bin/$(PROG) $(PREFIX)/bin/
	cp doc/$(PROG) $(PREFIX)/doc/
uninstall:
	rm $(PREFIX)/bin/$(PROG)
	rm $(PREFIX)/doc/$(PROG)